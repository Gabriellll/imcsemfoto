package com.example.imcgarcia;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText editTextAltura;
    TextView txtResultado;
    EditText editTextPeso;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtResultado = findViewById(R.id.TxtResultado);
        editTextAltura = findViewById(R.id.editTextAltura);
        editTextPeso = findViewById(R.id.editTextPeso);
    }

    public void Calcular (View view) {

        double Altura = Double.parseDouble(editTextAltura.getText().toString());
        double Peso = Double.parseDouble(editTextPeso.getText().toString());
        double IMC = Peso/(Altura*Altura);

        if (IMC < 18.5){

            txtResultado.setText("Abaixo do peso");

        } else if ( IMC < 24.9){

            txtResultado.setText("Peso Ideal");

        } else if (IMC < 29.9){

            txtResultado.setText("Levemente acima do peso");

        } else if ( IMC < 34.9) {

            txtResultado.setText("Obesidade grau 1");

        } else if ( IMC < 39.9) {

            txtResultado.setText("Obesidade grau 2 (Severa)");

        } else {

            txtResultado.setText("Obesidade grau 3 (Mórbida)");

        }
    }
}
